# Sets cars equal to 100
cars = 100
# Sets sapce_in_car equal to 4.0
space_in_a_car = 4.0
# sets drivers equal to 30
drivers = 30
# sets passengers equal to 90
passengers = 90
# sets cars_not_driven equal to 100 - 30
cars_not_driven = cars - drivers
# sets cars_driven = 30
cars_driven = drivers
# sets carpool_capacity equal to 280.0
carpool_capacity = cars_driven * space_in_a_car
# sets average_passengers_per_car = 3
average_passengers_per_car = passengers / cars_driven
# prints
puts "There are #{cars} cars available."
puts "There are only #{drivers} drivers available."
puts "There will be #{cars_not_driven} empty cars today."
puts "We can transport #{carpool_capacity} people today."
puts "We have #{passengers} passengers to carpool today."
puts "We need to put about #{average_passengers_per_car} in each car."
